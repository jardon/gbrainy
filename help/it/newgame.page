<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" id="newgame" xml:lang="it">
      
  <info>
    <title type="sort">1</title>
    <link type="guide" xref="index#play"/>
    <link type="seealso" xref="gametypes"/>
    <desc>Iniziare una partita.</desc>
    <revision pkgversion="2.30" version="0.1" date="2010-01-10" status="draft"/>
    <revision pkgversion="2.30" version="0.2" date="2010-01-21" status="review"/>
   <credit type="author">
    <name>Milo Casagrande</name>
    <email>milo@ubuntu.com</email>
   </credit>
   <license>
     <p>Creative Commons - Condividi allo stesso modo 3.0</p>
   </license>
  </info>

  <title>Sessione di gioco</title>
  
  <section id="game-start">
    <title>Iniziare una nuova sessione di gioco</title>
    <p>Per iniziare una nuova partita, eseguire una delle seguenti operazioni:</p>
    <list>
      <item>
        <p>Scegliere <guiseq><gui style="menu">Partita</gui><gui style="menuitem">Nuova partita</gui></guiseq> e selezionare il tipo di rompicapo a cui giocare.</p>
      </item>
      <item>
        <p>Fare clic su uno dei pulsanti nella barra degli strumenti.</p>
      </item>
    </list>
    <p>Il significato dei pulsanti nella barra degli strumenti è il seguente:</p>
    <table frame="none" rules="none" shade="none">
      <tr>
        <td>
        <terms>
          <item>
            <title>Tutti</title>
            <p>Inizia una nuova partita con tutti i tipi di gioco disponibili.</p>
          </item>
        </terms>
        </td>
        <td>
          <media type="image" mime="image/png" src="figures/all-games.png" its:translate="no">
            <p>
              All games button
            </p>
          </media>
        </td>
      </tr>
      <tr>
        <td>
        <terms>
          <item>
            <title>Logica</title>
            <p>Inizia una nuova partita con i soli giochi di logica.</p>
          </item>
        </terms>
        </td>
        <td>
          <media type="image" mime="image/png" src="figures/logic-games.png" its:translate="no">
            <p>
            Logic games button
            </p>
          </media>
        </td>
      </tr>
      <tr>
        <td>
        <terms>
          <item>
            <title>Calcolo</title>
            <p>Inizia una nuova partita con i soli giochi di calcolo.</p>
          </item>
        </terms>
        </td>
        <td>
          <media type="image" mime="image/png" src="figures/math-games.png" its:translate="no">
            <p>
              Calculation games button
            </p>
          </media>
        </td>
      </tr>
      <tr>
        <td>
        <terms>
          <item>
            <title>Memoria</title>
            <p>Inizia una nuova partita con i soli giochi di memoria.</p>
          </item>
        </terms>
        </td>
        <td>
          <media type="image" mime="image/png" src="figures/memory-games.png" its:translate="no">
            <p>
              Memory games button
            </p>
          </media>
        </td>
      </tr>
      <tr>
        <td>
        <terms>
          <item>
            <title>Verbale</title>
            <p>Inizia una nuova partita con i soli giochi di analogia verbale.</p>
          </item>
        </terms>
        </td>
        <td>
          <media type="image" mime="image/png" src="figures/verbal-games.png" its:translate="no">
            <p>
              Verbal games button
            </p>
          </media>
        </td>
      </tr>
    </table>
    <note>
      <p>Queste descrizioni si applicano anche ai tipi di gioco che è possibile selezionare dal menù <gui style="menu">Partita</gui>.</p>
    </note>
  </section>
  <section id="game-play">
    <title>Gioca una sessione</title>
    <note style="tip">
      <p>Quando si gioca, leggere sempre attentamente le istruzioni!</p>
    </note>
    <p>La sessione di gioco inizia mostrando il problema e ponendo una domanda. Sul fondo della finestra sono presenti i controlli principali da usare per interagire con il gioco.</p>
    <p>Quando si conosce la risposta al problema, digitarla nella casella di testo <gui style="input">Risposta</gui> e premere <gui style="button">OK</gui>.</p>
    <p>Per procedere al gioco successivo, fare clic su <gui style="button">Successivo</gui>.</p>
    <note>
      <p>Se viene selezionato <gui style="button">Successivo</gui> prima di completare il gioco o senza fornire una risposta, verrà conteggiato lo stesso nei propri risultati.</p>
    </note>
  </section>
</page>
