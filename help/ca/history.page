<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="history" xml:lang="ca">
  <info>
    <link type="guide" xref="index#play"/>
    <desc>Accediu a l'historial personal de partides.</desc>
    <revision pkgversion="2.30" version="0.1" date="2010-01-22" status="draft"/>
    <credit type="author">
      <name>Milo Casagrande</name>
      <email>milo@ubuntu.com</email>
    </credit>
    <license>
      <p>Reconeixement-Compartir Igual 3.0</p>
    </license>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas i Hernàndez</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2009-2018</mal:years>
    </mal:credit>
  </info>

  <title>Historial personal de partides del jugador</title>
  
  <p>El quadre de diàleg <gui>Historial de partides del jugador</gui> mostra el vostre rendiment per a cada tipus de joc durant les darreres partides.</p>
  
  <section id="history-personal">
    <title>Visualitzar l'historial del jugador</title>
    <p>Per a accedir a l'historial de partides del jugador, seleccioneu <guiseq> <gui style="menu">Visualitza</gui><gui style="menuitem">Historial de partides del jugador</gui></guiseq>.</p>
  </section>
  
  <section id="history-change-graphic">
    <title>Seleccionar quins resultats mostrar</title>
    <p>Per a seleccionar els resultats que es mostraran al gràfic, utilitzeu els diferents quadres de verificació ubicats sota el gràfic.</p>
  </section>
  
  <section id="history-save-prefs">
    <title>Canviar el nombre de jocs desats</title>
    <p>El <app>gbrainy</app> desa les puntuacions del jugador per a poder fer un seguiment de la seva progressió.</p>
    <p>Per tal de canviar el nombre de partides que s'emmagatzemen en l'historial, seguiu els passos següents:</p>
    <steps>
      <item>
        <p>Seleccioneu <guiseq><gui style="menu">Paràmetres</gui><gui style="menuitem"> Preferències</gui></guiseq>.</p>
      </item>
      <item>
        <p>A la secció <gui>Historial de partides del jugador</gui>:</p>
        <list>
          <item>
            <p>Utilitzeu el botó de selecció de valor per a canviar quantes partides cal que s'emmagatzemin abans que comencin a mostrar-se els resultats al gràfic de l'historial.</p>
          </item>
          <item>
            <p>Utilitzeu el segon botó de selecció de valor per a canviar quantes partides es mostraran al gràfic de l'historial.</p>
          </item>
        </list>
      </item>
    </steps>
  </section>
</page>
